# Visualize the share of total CO2 emitted within different generations

This script can be run to rebuild and update this powerful image.

<img src="co2_generations_graph.png" width="500" height="400">

It uses data from Our World in Data [link](https://ourworldindata.org/grapher/cumulative-co-emissions).

Output looks like this:

<img src="output.jpg" width="500" height="400">

# Todo
- [ ] Make the graph interactive, so that you can move the line and summary to your own generation.

